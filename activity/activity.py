# ACTIVITY:

# 1. Create an abstract class called Animal that has the following abstract methods:

# a. eat(food)

# b. make_sound()

# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:

# a. Properties:
# i. Name
# ii. Breed
# iii. Age

# b. Methods:
# i. Getters
# ii. Setters
# iii. Implementation of abstract methods
# iv. call()

# ANSWER:
from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass


class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"{self._name} the cat is eating {food}.")

    def make_sound(self):
        print(f"{self._name} the cat says meow.")

    def call(self):
        print(f"{self._name} the cat comes running.")

# Test case:
cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()



class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f"{self._name} the dog is eating {food}.")

    def make_sound(self):
        print(f"{self._name} the dog says woof.")

    def call(self):
        print(f"{self._name} the dog comes running.")

# Test case:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat ("Steak")
dog1.make_sound()
dog1.call()
