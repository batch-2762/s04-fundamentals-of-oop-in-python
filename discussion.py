# FUNDAMENTALS OF OOP

class Sample_Class():
	# we use () para sa inheritance.
	# pwede rin walang ()
	# __init___ means to inherit

	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

sample_obj = Sample_Class(2023)

print(sample_obj.year)
sample_obj.show_year()

# [Section] Fundamentals of OOP
# Four pillars of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstaction

# First Pillar: Encapsulation
# Encapsulation is a mechanism of wrappin the attributes and code acting on the methods together as a single
#isang underscore means protected, or mor on informing or code readability.
# data hiding pero hindi sila nagtatago, angtatagoan lng sila.
class Person():
	def __init__(self, name, age):
		self._name = name
		self._age = age

	# Methods
	# getter of _name attribute
	def get_name(self):
		print(f'Name of person: {self._name}')

	# setter of _name attribute
	def set_name(self, name):
		self._name = name


	# getter of _age attribute
	def get_age(self):
		print(f'Age of ferson: {self._name}')

	# setter of _age attribute
	def set_age(self, age):
		self._age = age 

# new instance
person_one = Person("James", 245)

person_one.get_name()

person_one.set_name("Kat")

person_one.get_name()

person_one.get_age()

person_one.set_name(3)
person_one.get_age()
# print(person_one.name) #para ma access


# Second Pillar: Inheritance
# THe inherittance of the characteristic or attributes ofa parent class to a child class that are derived from it.

# Syntax: class child_class_name(parent_class_name) 

class Employee(Person):
	def __init__(self, name, age, _employee_id):
		# super() can be used to invoke immediate parent class contructor
		super().__init__(name, age)

		# pag naglagay tayo ng self super().__init__(self, name, age) it means na nag iinherit tayo kay Employee.
		#basically, parang contained na sina self._name at self._age diyan kay super().__init__(name, age). Ans: YES

		self._employee_id = _employee_id

	# method for the _employee_id attribute
	def get_employee_id(self):
		print(f'The emplyee ID is {self._employee_id}')

	def set_employee_id(self, employee_id):
		self._employee_id = employee_id

employee_one = Employee("CongTV", 27, "0001")

employee_one.get_age()
employee_one.set_age(28)
employee_one.get_age()

employee_one.get_name()
employee_one.set_name("Lincoln")
employee_one.get_name()

employee_one.get_employee_id()
employee_one.set_employee_id("legend")
employee_one.get_employee_id()


# Third Pillar: Polymorphism (Poly means more, morph means to form)
# Functions and objects

class Team_Lead():
	def occupation(self):
		print('Team Lead')

	def has_auth(self):
		print(True)

class Team_Member():
	def occupation(self):
		print('Team Member')

	def has_auth(self):
		print(False)

team_lead = Team_Lead()
team_member = Team_Member()

for person in (team_lead, team_member):
	person.occupation()


# Polymorphism with Inheritance
# Polymorphism in python defines methods in the child class that have the same name as the methods in the parent class.

class Zuitt():
	def tracks(self):
		print('We are currenlt offering 3 tracks(developer career, pi-share career and short courses)')

	def num_of_hours(self):
		print('Learn web development in 360 hours!')

class Developer_Career(Zuitt):
	# overriding of the parent method
	def num_of_hours(self):
		print('Learn the basics of web dev in 240 hours!')

class Pi_Shape_Career(Zuitt):
	def num_of_hours(self):
		print('Learn skills for no-code app dev in 140 hours!')

class Short_Courses(Zuitt):
	def num_of_hours(self):
		print('Learn advanced topics in web dev in 20 hours!')

course_one = Developer_Career()
course_two = Pi_Shape_Career()
course_three = Short_Courses()

for course in (course_one, course_two, course_three):
	course.tracks()
	course.num_of_hours()

# Fourth Pillar: Abstraction
# An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within any child classes built from abstract class.

from abc import ABC
# This import tells the program to get abc modile of python to be used. # Abstract or walang laman.
# ABC means abstract base classes.

class Polygon(ABC):
	def print_number_of_sides(self):
		# The pass keyword it denotes that the method doesn't do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This polygon has 3 sides')

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()
	def print_number_of_sides(self):
		print(f'This polygon has 5 sides')

shape_one = Triangle()
shape_two = Pentagon()
shape_one.print_number_of_sides()
shape_two.print_number_of_sides()

#properties are 
#   def __init__(self):
#	super().__init__()